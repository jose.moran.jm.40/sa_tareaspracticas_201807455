
import { useEffect, useState } from 'react'
import { toast } from 'react-toastify';
import { ToastContainer } from 'react-toastify';
import axios from 'axios'
import './App.css';
import 'react-toastify/dist/ReactToastify.css';
import 'bootstrap/dist/css/bootstrap.min.css';


function App() {
  

  const [reporte, setReporte] = useState({
    Carnet: '',
    Nombre: '',
  })
  const [lista, setLista] = useState([])

  const onChange = (event) => {
    const { name, value } = event.target;
    setReporte((prevReporte) => ({
      ...prevReporte,
      [name]: value,
    }))
  }
  const agregarReporte = () => {
    if (!reporte.Carnet) return alert('Ingrese todos los campos')
    if (!reporte.Nombre) return alert('Ingrese todos los campos')
    const URL = process.env.REACT_URL
    console.log(URL)

    axios.post(`/server/addStudent`, {
      name: reporte.Carnet,
      carnet: reporte.Nombre,
    }).then(response=> {toast.success(response.data.message);getStudents()})
    

    return 

  }
//
  useEffect(()=>{
    getStudents()
  },[])


  const getStudents = () =>{
    axios.get(`/server/getStudents`).then(response=> setLista(response.data))
  }

  return (
    <div style={{
      display: 'flex', justifyContent: 'center', flexDirection: 'column', alignItems: 'center', padding: 40
    }}>
      <h2>Agregar Estudiante</h2>
      <div style={{ display: 'flex' }} name="form">
        <div>
          <div className="input-container">
            Carnet
            <input type="text" name="Carnet" onChange={onChange}></input>
          </div>
          <div className="input-container">
            Nombre
            <input type="text" name="Nombre" onChange={onChange}></input>
          </div>

          <div style={{ display: 'flex', flexDirection: "column", width: 400, padding: '20px 7px' }}>
            <button className="btn btn-dark" name="boton" onClick={agregarReporte}>Agregar</button>
          </div>
        </div>
      </div>
      <div style={{ display: 'flex', flexWrap: 'wrap', maxWidth: '1000px', gap: '20px' }}>
        {lista.map((i, key) => {
          return <div style={{ background: 'white', padding: '2rem', color: 'black', borderRadius: '10px' }}>
            <p>Carnet: {i.carnet} </p>
            <p>Nombre: {i.name} </p>
          </div>
        })}
      </div>

      <ToastContainer autoClose={5000} hideProgressBar />
    </div>
  )

}

export default App;
