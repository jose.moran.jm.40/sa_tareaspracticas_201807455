from flask import Flask, request, jsonify, abort
import requests
import json
import mysql.connector
from flask_cors import CORS
import os
from dotenv import load_dotenv


app = Flask(__name__)
cors = CORS(app, resources={r"/*": {"origins": "*"}})

def Conection():
    HOST_DB = os.environ.get('HOST_DB')
    if HOST_DB is None:
        HOST_DB = '10.10.12.2'
    USER_DB = os.environ.get('USER_DB')
    if USER_DB is None:
        USER_DB = 'root'
    PASSWORD_DB = os.environ.get('PASSWORD_DB')
    if PASSWORD_DB is None:
        PASSWORD_DB = 'pass'
    print(HOST_DB)
    print(USER_DB)
    print(PASSWORD_DB)

    mydb = mysql.connector.connect(
        host=HOST_DB,
        user=USER_DB,
        password=PASSWORD_DB,
        database="tp9"
    )
    return mydb




@app.route('/server/addStudent', methods=['POST'])
def addStudent():
    content = request.get_json(force=True)
    try:
        name = content['name']
        carnet = content['carnet']
        print(name)
        mydb = Conection()
        cursor = mydb.cursor()
        query = "INSERT INTO student VALUES (%s, %s)"
        values = (carnet, name)

        cursor.execute(query, values)
        mydb.commit()


        return jsonify({"message": "Registrado"})

    except Error:
        print(Error)
        return jsonify({"message": "Error"})


@app.route('/server/getStudents', methods=['GET'])
def getStudents():
    mydb = Conection()
    cursor = mydb.cursor()
    query = "select * from student"
    
    cursor.execute(query)
    response = cursor.fetchall()

    data = []
    for row in response:
        data.append({"name": row[0], "carnet": row[1]})


    return jsonify(data)

